+++
description = ""
draft = false
date = "2017-11-10T11:51:27-02:00"
title = "Certificados de Participação"
tags = [ "DevOps", "Certificados" ]
categories = [ "Announcement", "News" ]
image = "img/home-bg.jpg"

+++

Consideramos emitir o **Certificado de Participação** do evento por ser importante a um evento técnico como foi este **DevOpsDays Brasília 2017**.

Assim, disponibilizamos a partir de hoje a emissão do certificado de participação no **DevOpsDays Brasília 2017**.

Para ter acesso ao seu certificado informe o email utilizado na inscrição do evento no campo a seguir. Lembrando que apenas aqueles que tiveram a sua presença confirmada no evento terão acesso ao seu respectivo certificado.

<iframe
  allowtransparency="true"
  border="0"
  frameborder="0"
  src="http://www.eventize.com.br/eventize/frm_gera_certificado.php?SEQ_EVE=4201"
  style="border:0;margin:0;padding:0;width:100%;background-color: none;height:150px;">
</iframe>

Mais uma vez o nosso muito obrigado pela sua presença.

Caso necessite de alguma informação adicional ou mesmo a correção dos dados do seu certificado, entre em contato com a organização através do email [`organizers-brasilia-2017@devopsdays.org`](mailto:organizers-brasilia-2017@devopsdays.org).

Nos vemos em 2018!
