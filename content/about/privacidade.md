+++
categories = ["Development", "Operation"]
date = "2016-09-01T18:11:45-03:00"
description = ""
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Política de privacidade"

+++

# Nossa Política de privacidade

Este site e outros que possam estar a ele vinculados - como o *Chamada de Trabalhos* e *Inscrições de Participantes* - podem utilizar ou armazenar cookies no seu computador durante o carregamento de uma página.

Estes recursos poderão ser usados na contagem de acessos ao site.

Os dados que podem ser obtidos incluem o endereço IP, informações sobre o navegador, a página de origem, o idioma e data do acesso entre outros.

Cookies podem ser excluídos ou bloqueados pelos usuário através das opções do navegador.

Para navegarem pelo site os visitantes não necessitam cadastrar qualquer informação pessoal.

Mediante *ordem judicial* seremos obrigado a fornecer informações que tenham sido registradas no nosso site, de outra forma comprometemos-nos a não fornecê-las.

Esta Política de Privacidade poderá ser alterada a qualquer tempo. Dessa forma, recomenda-se sua leitura periodicamente.

## Do palestrante

O cadastro realizado por palestrantes em formulário específico disponível no site será processado para as necessidades do processo de seleção como palestrante.

Das propostas aceitas os dados como nome, "mini-bio", foto, conta twitter do palestrante serã utilizados para divulgação do evento, bem como compor informações na grade de palestras do site do evento.

Os arquivos referentes às palestras como *slides* e registros de fotos ou vídeos realizados pelo DevOpsDays e/ou DevOpsDays Brasília (ou seus afiliados) poderão ser disponibilizados publicamente a critério do DevOpsDays Brasília.

## Dos participantes em geral

Participante é considerado como todo indivíduo que estiver no local do evento independentemente de haver realizado inscrição para participar de palestras, ignites, open spaces etc.

A inscrição para o evento deve ser realizada em formulário específico disponível no site, esta inscrição é de responsabilidade de cada inscrito que deve realizar o correto preenchimento. Os dados registrados naquele formulário serão processados e armazenados para as necessidades do processo de inscrições e comprovação de respectivos pagamentos.

Os registros de fotos ou vídeos realizados pelo DevOpsDays e/ou DevOpsDays Brasília (ou seus afiliados), contendo imagem que referencie qualquer dos participantes poderão ser disponibilizados publicamente a critério do DevOpsDays Brasília - inclusive para fins de uso relacionados ao [**Código de Conduta**](about/conduta) disponível no site do evento.
