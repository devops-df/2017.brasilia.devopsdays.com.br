+++
categories = ["Development", "Operation"]
date = "2016-08-26T20:48:04-04:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "O programa"
description = "07 de novembro"

+++

Teremos três formas de sessões:

1. Palestra de 25 minutos
1. Debate ou painel de 25 minutos
1. Mini-Palestra de 12 minutos

Durante o evento também teremos espaços para:

- **Ignições**: serão apresentadas durante as sessões *ignite*^[***ignite***: <https://www.devopsdays.org/ignite-talks-format/>]. Serão 4 sessões com até  7 minutos cada e com slides trocando automaticamente. Estes ignites acontecerão no auditório principal.
- **Sessões de Espaço Aberto**: As sessões iniciadas nos *ignites* serão levadas para os espaços abertos (*open spaces*^[***open spaces***: <https://www.devopsdays.org/pages/open-space-format>]) onde a discussão irá se aprofundar naqueles temas.
- **Roda Viva**: Oportunidades de debates de ampla participação do plublico presente como debatedor presente no centro da Roda (Open Fishbowl^[***Fishbowl***: <https://en.wikipedia.org/wiki/Fishbowl_(conversation)>])


<div class = "row">
  <div class = "col-md-4">
    <h3>Temas das trilhas</h3>
  </div>
</div>
<div class = "row">
  <div class = "col-md-4">
  </div>
  <div class = "col-md-12">
      <ul class="trilhas">
        <li class="box-blue"><strong>C</strong><br>Culture<br><span>(Cultura)</span></li>
        <li class="box-navy"><strong>A</strong><br>Automation<br><span>(Automação)</span></li>
        <li class="box-purple"><strong>M</strong><br>Metrics<br><span>(métricas, monitoramento, gestão)</span></li>
        <li class="box-orange"><strong>S</strong><br>Sharing<br><span>(compartilhamento)</span></li>
      </ul>
  </div>
</div>

### Abertura
{{< youtube id="LmmFEuNzPVk" autoplay="false" >}}

<div class = "row">
  <div class = "col-md-12 text-center">
    <h2>A programação de palestras e workshops</h2>
  </div>
</div>

<div class = "row programacao">

  <div class="row">
    <div class="col">

    <span class="tabs">
    <a href="#" class="current">1&ordm; Dia: 07 de novembro</a>
    <a href="/about/programa2">2&ordm; Dia: 08 de novembro</a>
    </span>


      <p class="text-center">Esta é a programação para o 1&ordm; Dia: 07 de novembro</p>
    </div>
    <div class="col-md-12">

      <div class="row">
        <div class = "col-md-4 col-md-offset-0">
          <time>08:00-12:00</time>
          <room></room>
        </div>
        <div class = "col-md-8 box">
          Credenciamento
        </div>
      </div>
    </div>
  </div>
  <div class = "col-md-6">
    <div class = "row">
      <div class = "col-md-12">

        <h3 class="text-center">Palestras (Jequitiba)</h3>

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>09:00-09:35</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Abertura</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>09:35-10:10</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 1</i> <b class="text text-blue">[C]</b><br> <br />
            <a href="/program/guto-carvalho" class="text text-blue">
              DevOps State of Union 2017<br />
              <strong>Palestrante:</strong><br> Guto Carvalho<br />
              <strong>Empresa:</strong><br> DevOpsDays Brasília Local Team <br />
              <strong>Disponível mídia:</strong> Slides e Vídeo<br />
            </a>
          </div>
        </div> <!-- end timeslot div  xxxxxxx -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:10-10:45</time>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Coffee-break</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:45-11:20</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 2</i> <b class="text text-navy">[A]</b><br> <br />
            <a href="/program/igor-souza" class="text text-navy">
              "You build it, you run it" - Mantendo sua infraestrutura como parte da aplicação!<br />
              <strong>Palestrante:</strong><br> Igor Souza<br />
              <!--<strong>Empresa:</strong><br> -->
              <strong>Disponível mídia:</strong> Slides e Vídeo<br />
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>11:20-11:55</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 3</i> <b class="text text-orange">[S]</b><br> <br />
            <a href="/program/bruno-dantas" class="text text-orange">
              DevSecOps: Adotando uma cultura de segurança ágil<br />
              <strong>Palestrante:</strong><br> Bruno Dantas<br />
              <!--<strong>Empresa:</strong><br> -->
              <strong>Disponível mídia:</strong> Slides e Vídeo<br />
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>11:55-12:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 4</i> <b class="text text-navy">[A]</b><br> <br />
            <a href="/program/luiz-henrique" class="text text-navy">
              Automação de processos em empresas tradicionais<br />
              <strong>Palestrante:</strong><br> Luiz Henrique<br />
              <strong>Empresa:</strong> Movida<br />
              <strong>Disponível mídia:</strong> Slides e Vídeo<br />
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>12:30-14:15</time>
            <room>livre</room>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Almoço livre</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>09:00-17:00</time>
            <room><i>showcase room</i></room>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Demonstrações</b><br> <br />
            Vejam o que os nossos patrocinadores têm para nos mostrar
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>14:15-14:50</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 5</i> <b class="text text-blue">[C]</b><br> <br />
            <a href="/program/mateus-prado" class="text text-blue">
              DevOps Behind The Scenes<br />
              <strong>Palestrante:</strong><br> Mateus Prado<br />
              <!--<strong>Empresa:</strong><br> -->
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>14:50-15:25</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 6</i> <b class="text text-navy">[A]</b><br> <br />
            <a href="/program/fernando-boaglio" class="text text-navy">
              Jenkins por dentro e por fora<br />
              <strong>Palestrante:</strong><br> Fernando Boaglio<br />
              <strong>Empresa:</strong><br> Keyrus Digital
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>15:25-16:00</time>
            <room></room>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Coffee-break</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>16:00-16:35</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 7</i> <b class="text text-navy">[A]</b><br>
            <a href="/program/misael-santos" class="text text-navy">
              Pipelines Turbinadas: Impulsionando a cultura DevOps com Contêineres, Qualidade e Segurança na nuvem<br />
              <strong>Palestrante:</strong> Misael Santos<br />
              <strong>Empresa:</strong> SERPRO<br>
              <strong>Disponível mídia:</strong> Slides e Vídeo<br />
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>16:35-17:10</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 8</i> <b class="text text-navy">[A]</b><br> <br />
            <a href="/program/cristiano-gomes" class="text text-navy">
              Cloud DevOps Mobile<br />
              <strong>Palestrante:</strong><br> Cristiano Gomes<br />
              <strong>Empresa:</strong><br> Microsoft
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>17:10-17:45</time>
            <room></room>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Encerramento</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

      </div>
    </div>
  </div><!-- end day 1 -->

  <div class = "col-md-6">
    <div class = "row">
      <div class = "col-md-12">
        <h3 class="text-center">Oficinas || Hacklab || Coding</h3>

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>09:00-10:10</time>
          </div>
          <div class = "col-md-8 box-workshop">
            <b class="text text-default">livre</b><br> <br />
            Acompanhe a abertura e a primeira palestra
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:10-10:45</time>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Coffee-break</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:30-11:30</time>
            <room>Sala IPÊ</room>
          </div>
          <div class = "col-md-8 box-workshop">
            <i>Slot 1</i> <b class="text text-orange">[S]</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>11:30-12:30</time>
            <room>Sala IPÊ</room>
          </div>
          <div class = "col-md-8 box-workshop">
            <i>Slot 2</i> <b class="text text-blue">[C]</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>12:30-14:15</time>
            <room>livre</room>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Almoço livre</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>09:00-17:00</time>
            <room><i>showcase room</i></room>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Demonstrações</b><br> <br />
            Vejam o que os nossos patrocinadores têm para nos mostrar
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>14:30-15:30</time>
            <room>Sala IPÊ</room>
          </div>
          <div class = "col-md-8 box-workshop">
            <i>Slot 3</i> <b class="text text-orange"> [S]</b><br> <br />
            <a href="/program/rafael-benevides" class="text text-orange">
              Kubernetes for Docker users<br />
              <strong>Palestrante:</strong><br> Rafael Benevides<br />
              <strong>Empresa:</strong><br> Red Hat
            </a>
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>15:25-16:00</time>
          </div>
          <div class = "col-md-8 box">
            <b class="text text-default">Coffee-break</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>16:00-17:00</time>
            <room>Sala IPÊ</room>
          </div>
          <div class = "col-md-8 box-workshop">
            <i>Slot 4</i> <b class="text text-navy"> [A]</b><br> <br />
          </div>
        </div> <!-- end timeslot div -->

      </div>
    </div>
  </div><!-- end day 2 -->

  <!-- confraternização DevOpsDays -->

  <div class="ending">
  <time>19:00...</time> <br />
  <room>TBD</room>

  <h3>Confraternização DevOpsDays Brasília</h3>
  com amigos novos e de longa data <br><br>
  <i class="fa fa-heart fa-2x"> </i> <i class="fa fa-smile-o fa-2x"> </i>

  </div>



</div>

<small>Mudanças na grade podem ocorrer decorrentes de circunstâncias fora do controle desta organização. Em especial
quando for algo relacionado a agenda de trabalho de palestrantes ou questões de ordem pessoal dos mesmos. Nestes casos a organização vai remanejar o slot inserindo outro conteúdo no mesmo horário.</small>
