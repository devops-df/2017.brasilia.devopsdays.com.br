+++
categories = ["Announcement", "News"]
date = "2017-07-25T22:54:45-03:00"
description = "Participe do DevOpsDays Brasília 2017"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Compartilhamento", "Inscrições"]
title = "Inscreva-se!"

+++

Venha participar da edição 2017 do DevOpsDays de Brasília!

Teremos várias palestras e atividades para dois dias inteiros de troca de experiências entre os vários presentes e palestrantes.

## Sobre as inscrições

Para *pessoas com deficiência*^[*pessoas com deficiência:* considera-se pessoa com deficiência aquela que tem impedimentos de longo prazo de natureza física, mental, intelectual ou sensorial, os quais, em interação com diversas barreiras, podem obstruir sua participação plena e efetiva na sociedade em igualdade de condições com as demais pessoas. - `LCP 142/13`] será dado **50% de desconto**. Para estes casos entre em contato pelo e-mail <b>organizers-brasilia-2017@devopsdays.org</b> solicitando mais informações.

Registre-se^[***Eventize*** é o nosso provedor de serviço para registro e gerenciamento de inscrições] o quanto antes e aproveite o desconto para inscrições antecipadas.

<table class="table table-hover table-condensed table-bordered">
  <tr><th colspan=2 class="text-center"><h3>Investimento</h3></th></tr>

  <tr>
    <th>EarlyBird (até 20/08/2017, limitado a 25 inscritos)</th>
    <th class="text-center">Valor (R$)</th>
  </tr>
  <tr>
    <td>- Normal</td>
    <td class="text-right">280,00</td>
  </tr>

  <tr>
    <th>Participante (até 20/10/2017)</th>
    <th class="text-center">Valor (R$)</th>
  </tr>
  <tr>
    <td>- Normal</td>
    <td class="text-right">350,00</td>
  </tr>

  <tr>
    <th>Empenho* (grupo mínimo de 5 pessoas)</th>
    <th class="text-center">Valor (R$)</th>
  </tr>
  <tr>
    <td>- Normal</td>
    <td class="text-right">500,00</td>
  </tr>

</table>

<small>
<b>*</b> Caso necessite fazer inscrições para governo via **empenho**, estas devem ser feitas para grupo de no mínimo **5 (cinco) inscrições** e serão realizadas diretamente com a organização do evento. Entre em contato pelo email ***organizers-brasilia-2017@devopsdays.org*** solicitando informações detalhadas.
</small>

<iframe src="https://devopsdaysbrasilia2017.eventize.com.br/inscricao.php" id="frame_form" frameborder="0"
    border="0" marginheight="0" marginwidth="0" width="100%" height="800"
    hspace="0" vspace="0" align="top" allowtransparency = "true" style="border:0; margin:0;
    padding:0; width:100%; background-color: none; height:800px;">
</iframe>
