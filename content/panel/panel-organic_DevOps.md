+++
categories = ["Development", "Operation"]
date = "2016-10-23T19:44:52-02:00"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Painéis de debates"
description = "DevOps & Ciência de Dados"
panel = "organic_DevOps"
+++

# <span class="text text-primary">Operação Serenata de Amor</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**:<span class="text-primary">Culture (Cultura)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/anaschwendler/devopsdays`](https://speakerdeck.com/anaschwendler/devopsdays)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
A *Operação Serenata de Amor* surgiu a mais ou menos um ano e desde então tem tomado proporções muito interessantes.

Ana Schwendler vai falar um pouco sobre como o projeto chegou até aqui e como o time se construiu multidisciplinar, remoto, que codifica, mantém, sustenta e evolui o projeto de forma organizada e orgânica.

*Operação Serenata de Amor* é projeto de tecnologia que usa inteligência artificial para auditar contas públicas e combater a corrupção. A ideia surgiu do cientista de dados Irio Musskopf, ao perceber que ainda existiam muitas brechas no uso de tecnologia para fiscalizar gastos de parlamentares. Ao compartilhar a ideia com amigos, não demorou muito para o projeto ganhar força e um time formado por 8 pessoas dispostas a realizá-lo.

{{< youtube id="ASrk2eUaEkc" autoplay="false" >}}
