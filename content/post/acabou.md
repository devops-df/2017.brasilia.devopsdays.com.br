+++
title = "Até o ano que vem, galera!"
tags = [
  "DevOps",
  "Culture",
]
categories = [
  "Announcement",
  "News",
]
image = "img/home-bg.jpg"
description = "Felizmente está nos planos uma nova edição!"
draft = false
date = "2017-11-08T19:21:16-02:00"

+++

# Acabou!!! <i class="fa fa-frown-o" aria-hidden="true"></i>

Pois é! O DevOpsDays Brasília 2017 chegou ao fim!

Agradecemos a todos os envolvidos pelo bom andamento do evento e pela cooperação. <i class="fa fa-handshake-o" aria-hidden="true"></i>

Acreditamos que foi bastante proveitoso para todos e que aprendemos bastante!  <i class="fa fa-graduation-cap" aria-hidden="true"></i>

Não fiquem tristes, pois está nos planos uma versão 2018 do DevOpsDays Brasília. <i class="fa fa-smile-o fa-2x" aria-hidden="true"></i>

Logo logo... solicitaremos aos participantes que respondam nossa rápida pesquisa de opinião - não deixe de responder, pois será importante como insumo para nossa próxima edição.

Até a próxima!
