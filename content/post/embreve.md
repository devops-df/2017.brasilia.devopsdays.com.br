+++
image = "img/home-bg.jpg"
description = "Preparativos iniciados para o próximo evento"
draft = false
date = "2017-05-03T15:05:02-03:00"
title = "Edição 2017 em breve"
tags = [
  "DevOps",
  "Culture",
  "Automation",
  "Lean",
  "Metrics",
  "Sharing",
]
categories = [
  "Announcement",
  "News",
]

+++

# Dev & Ops novamente juntos em Brasília!

Os preparativos para a próxima edição do DevOpsDays Brasília foram iniciados.

No momento estamos em busca de local para sediar o evento que pretendemos realizar em **novembro/2017**. Para este ano estamos propondo um evento maior: de dois dias.

Com isso, nossos desafios de busca por patrocinadores será também maior, pois crescem bastante toda a logística e os custos para proporcionar um evento de alto nível como o que temos a intenção de promover para vocês.

Para este ano traremos mais novidades, então fiquem antenados à abertura das inscrições e à abertura proposta de trabalhos a serem apresentados no evento.
