+++
draft = false
date = "2017-11-10T13:37:06-02:00"
title = "Feedback Loop & Certificados"
tags = [ "DevOps", "Culture", "Lean" ]
categories = [ "Announcement", "News" ]
image = "img/home-bg.jpg"
description = "A gente adora receber contribuições em busca de melhorar continuamente o evento!"

+++

# Nós queremos lhe ouvir!

Uma das bases DevOps (e do DevOpsDays) é a participação colaborativa.

Consideramos que ao você responder a esta pesquisa^[Feedback Loop (https://www.scrumalliance.org/community/articles/2015/march/the-power-of-feedback-loops)] no formulário abaixo você irá ajudar para que o DevOpsDays Brasília venha a cada ano ser melhor para todos nós!

**Observação: Após responder a esta pesquisa você poderá acessar e emitir o seu certificado [clicando aqui](http://devopsdays.bsb.br/about/certificados)!**

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSc3oTHQixnE5_1ygMJLzM9NT_QepEuzhjx1MaV9VEczWEno9A/viewform" id="frame_form" frameborder="0"
    border="0" marginheight="0" marginwidth="0" width="100%" height="800"
    hspace="0" vspace="0" align="top" allowtransparency = "true" style="border:0; margin:0;
    padding:0; width:100%; background-color: none; height:800px;">
</iframe>
