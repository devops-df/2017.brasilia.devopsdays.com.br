+++
categories = ["Announcement", "News"]
date = "2017-11-17T12:11:13-02:00"
description = "Algumas das fotos e muitos momentos registrados..."
draft = false
image = "img/event-photo.png"
tags = ["DevOps", "Culture", "Sharing"]
title = "As fotos.."

+++

## Palestras, oficinas, coffee-break, as rodas de bate-bato e muito mais da galera presente aprendendo e ensinando!

<div id="flickrembed"></div>
<small style="display: block; text-align: center; margin: 0 auto;">
  Powered by <a href="https://flickrembed.com" target='blank'>flickr embed <i class='fa fa-external-link'></i></a>.
</small>
<script src='https://flickrembed.com/embed_v2.js.php?source=flickr&layout=responsive&input=www.flickr.com/photos/devops-df/albums/72157690674242776&sort=1&by=album&theme=default&scale=fit&limit=100&skin=default&autoplay=true'>
</script>
<div class="text text-center">
 <a href='https://www.flickr.com/photos/devops-df/albums/72157690674242776' target='blank'>
    Clique aqui e veja mais fotos <i class='fa fa-external-link'></i>
 </a><br />
 Local: Centro de Convenções do Hotel Grand Mercure, Brasília/DF
</div><br />
