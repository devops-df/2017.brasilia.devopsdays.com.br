+++
description = "John Willis interviewed"
draft = false
tags = [
  "DevOps",
  "Culture",
  "Automation",
  "Lean",
  "Metrics",
  "Sharing",
]
categories = [
  "Development",
  "Operation",
  "Announcement",
  "News",
]
image = "img/home-bg.jpg"
date = "2017-08-04T23:47:04-03:00"
title = "Mr DevOps"

+++

Entrevista a John Willis, por Miguel Di Ciurcio e Guto Carvalho

{{< youtube id="zzyRfeImHDI" autoplay="true" >}}

<small>Este vídeo foi usado para abrir o DevOpsDays Brasília 2016. Entrevista realizada durante a PuppetConf 2016.</small>
