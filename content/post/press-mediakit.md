+++
image = "img/home-bg.jpg"
description = "Um mediakit que mostra a nossa proposta de evento para 2017"
draft = false
date = "2017-09-01T11:24:28-03:00"
title = "Saiba + sobre o evento"
tags = [
  "DevOps",
  "Culture",
  "Folder",
  "MediaKit",
]
categories = [
  "Development",
  "Operation",
  "Announcement",
  "News",
]

+++

Disponibilizamos um folder que mostra a nossa proposta de evento para 2017 e informa o que foi o evento de 2016!

[<button type="button" class="btn btn-primary navbar-btn">Clique aqui e baixe sua cópia!</button>](/artes/2017/Press_mediakit-pt.pdf)

Mostre ao seus colegas e amigos para que também estão anciosos por conhecimento em métodos para a prática DevOps.

**Dica**:  Leve para conscientizar os chefes da importância da participação dele e das equipes
