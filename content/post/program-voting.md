+++
tags = [ "DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Voting" ]
categories = [ "Development", "Operation", "Announcement", "News" ]
image = "img/home-bg.jpg" #optional image - "img/home-bg.jpg" is the default
description = "Vote aqui! Venha votar pra gente fazer um evento melhor!"
title = "Ajude a gente a montar o programa"
draft = false
date = "2017-09-21T23:02:47-03:00"

+++

O DevOpsDays Brasília recebeu algumas dezenas de propostas de palestras.

Temos um comitê que irá realizar a seleção de palestras, mas também queremos a sua participação!

O principal objetivo do DevOpsDays é estimular o debate e a inovação com metodologias DevOps no Brasil.

Se você tem interesse de participar do evento como palestrante a chamadas (CFP) ainda estão abertas.

Para inscrever a sua proposta é só [clicar aqui](/about/cfp) e aguardar o retorno do nosso time de avaliadores :)

Enquanto isso lhe pedimos que marque o seu nível de interesse para cada uma do formulário abaixo.

Abraços e até lá!

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeRFYVNszjG4HGkd1OPBqEwiWX1v2Ek6lQXD0lG5phsQUmhYQ/viewform" width="800" height="1200" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>
