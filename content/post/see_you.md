+++
categories = ["Announcement", "News"]
date = "2017-12-12T09:10:00-02:00"
#PublishDate = "2016-11-18T20:10:00-02:00"
description = "Felizmente está nos planos uma nova versão!"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Registration", "Sharing"]
title = "Até o ano que vem!"

+++

# Acabou!!! <i class="fa fa-frown-o" aria-hidden="true"></i>

Pois é! O DevOpsDays Brasília 2017 terminou!

Mais uma vez o evento foi encerrado com chave de ouro!

Agradecemos a todos os envolvidos pelo bom andamento do evento e pela cooperação. <i class="fa fa-handshake-o" aria-hidden="true"></i>

Acreditamos que foi bastante proveitoso para todos e que aprendemos pra caramba!  <i class="fa fa-graduation-cap" aria-hidden="true"></i>

Não fiquem tristes, pois está nos planos uma versão 2018 do DevOpsDays Brasília. <i class="fa fa-smile-o fa-2x" aria-hidden="true"></i>

Por favor, [*clique aqui*](/post/feedback-loop) e respondam nossa rápida pesquisa de opinião - não deixe de responder, pois será importante como insumo para nossa próxima edição.

Ah! Como sabem o evento teve registro em [fotos](/post/fotos) e vídeo - os links para os vídeos estão disponíveis nas respectivas páginas das palestras - acesse a [programação](/about/programa) e assiste lá.
