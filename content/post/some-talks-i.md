+++
categories = [ "Announcement", "News" ]
image = "img/home-bg.jpg"
description = "As chamadas de trabalhos (CFP) ainda estão abertas, mas alguns palestrantes já estão definidos"
draft = false
tags = [ "DevOps", "Palestras", "Palestrantes" ]
date = "2017-09-25T16:27:11-03:00"
title = "Confira as primeiras palestras confirmadas para o DevOpsDays Brasília 2017"
+++

O DevOpsDays Brasília está quase chegando e a preparação para o evento está sendo intensa. Nossa organização já analisou dezenas de propostas para a realização de palestras e está identificando quais temas serão discutidos durante os dois dias da conferência. Os assuntos são diversos: teremos tanto palestras técnicas quanto painéis para discutir aspectos organizacionais da cultura DevOps.

Até o momento, já temos como palestrantes confirmados no evento:

- Adrian Goins (Rancher)  
  *Distributed Storage with Rancher-NFS and Syncthing*
- Bruno Dantas  
  *DevSecOps: Adotando uma cultura de segurança ágil*
- Diego Dorgam (Rocket.Chat)  
  *ChatBots com Linguagem Natural OpenSource com Rocket.Chat*
- Fábio Kaiser Rauber  
  *Docker, Rancher e a Hospedagem de Produtos Interlegis*
- Fernando Boaglio  
  *Jenkins por dentro e por fora*
- Fernando Ike  
  *Blameless: A culpa não é sua*
- Guto Carvalho (Instruct)  
  *DevOps State of Union 2017*
- Jairo da Silva Junior (Red Hat)  
  *Testes de Infraestrutura na Prática*
- Lucas Moreira Rodrigues Costa (Globo.com)  
  *A arquitetura de monitoramento da plataforma de vídeos da Globo.com*
- Marcos Macedo  
  *Fazendo entregas contínuas no governo federal*
- Mateus Prado  
  *DevOps Behind The Scenes*
- Misael da Silva Santos  
  *Pipelines Turbinadas: Impulsionando a cultura DevOps com Contêineres, Qualidade e Segurança na nuvem*
- Scott Moore (Microsoft)  
  *Security first: A SecDevOps approach to cloud building.*

Estas palestras estarão distribuídas nos dois dias de evento - [clique aqui para ver a progamação](/about/programa)

O principal objetivo do DevOpsDays é estimular o debate e a inovação com metodologias DevOps no Brasil. Se você tem interesse em participar do evento como palestrante e apresentar um trabalho com um assunto relevante para a área, a chamada de trabalhos (CFP) ainda está aberta. Para inscrever a sua proposta é só [clicar aqui](/about/cfp), enviar sua proposta e aguardar o retorno do nosso time de avaliadores :)

Para realizar a sua inscrição como participante e garantir o seu ingresso, [clique aqui](/about/registration).
