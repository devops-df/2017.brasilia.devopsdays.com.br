+++
categories = [ "Announcement", "News" ]
image = "img/home-bg.jpg"
description = "Inscreva-se e participe desta oportunidade ímpar para troca de experiências com os palestrantes ou participar de oficinas!"
draft = false
tags = [ "DevOps", "Palestras", "Palestrantes" ]
date = "2017-10-25T12:34:11-03:00"
title = "Confira mais algumas das palestras confirmadas para o DevOpsDays Brasília 2017"
+++

Faltam poucos dias para o DevOpsDays Brasília 2017 e a preparação para o evento continua intensa. Nossa organização já analisou dezenas de propostas para a realização de palestras e oficinas/wokshops a serem apresentadas durante os dois dias da conferência.

Teremos tanto palestras técnicas quanto painéis para discutir aspectos organizacionais da cultura DevOps.

Veja a lista de novos palestrantes confirmados no evento:

- Gustavo Luszczynski (Red Hat)  
  [*Desenvolvimento Ágil de Aplicações utilizando o OpenShift* ***(workshop)***](/program/gustavo-luszczynski)
- Igor Souza  
  [*“You build it, you run it” - Mantendo sua infraestrutura como parte da aplicação!*](/program/igor-souza)
- Jonathan Dias Baraldi (BRCloud)  
  [*DevOps com Kubernetes* ***(workshop)***](/program/jonathan-baraldi)
- Luiz Henrique de Souza (Movida)  
  [*Automação de processos em empresas tradicionais*](/program/luiz-henrique)
- Vinícius de Faria Silva (Ministério do Planejamento)  
  [*Uso de nuvens públicas pelo setor público: como evitar o aprisionamento nos provedores de serviços de nuvem?*](/program/vinicius-silva)

Estas palestras estarão distribuídas nos dois dias de evento - [clique aqui para ver a progamação](/about/programa)

O principal objetivo do DevOpsDays é estimular o debate e a inovação com metodologias DevOps no Brasil. Se você tem interesse em participar do evento como palestrante e apresentar um trabalho com um assunto relevante para a área, a chamada de trabalhos (CFP) ainda está aberta. Para inscrever a sua proposta é só [clicar aqui](/about/cfp), enviar sua proposta e aguardar o retorno do nosso time de avaliadores :)

Para realizar a sua inscrição como participante e garantir o seu ingresso, [clique aqui](/about/registration).
