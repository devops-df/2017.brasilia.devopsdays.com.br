+++
draft = false
date = "2017-11-12T13:37:06-02:00"
title = "Apresentações de 2017 disponibilizadas"
tags = [ "DevOps", "Culture", "Lean" ]
categories = [ "Announcement", "News" ]
image = "img/home-bg.jpg"
description = "Aêêê! Adicionamos à estante DevOps-DF as palestras do DevOpsDays Brasília 2017..."

+++

O grupo DevOps-DF <i class="fa fa-handshake-o" aria-hidden="true"></i> criou um espaço para que sejam disponibilizadas as palestras do DevOpsDays Brasília 2017!

Com isso, colocaremos lá os arquivos das apresentações que foram dadas no DevOpsDays Brasília 2016.

Veja na programação do evento ou para cada um de nossos palestrantes o link para as [palestras](/about/programa).

Por enquanto os palestrantes que disponibilizaram os slides foram:

- [Adrian Goins](/program/adrian-goins)
- [Ana Schwendler](/program/ana-schwendler)
- [Bruno Dantas](/program/bruno-dantas)
- [Diego Dorgam](/program/diego-dorgam)
- [Fabio Rauber](/program/fabio-rauber)
- [Fernando Ike](/program/fernando-ike)
- [Guto Carvalho](/program/guto-carvalho)
- [Igor Souza](/program/igor-souza)
- [Lucas Costa](/program/lucas-costa)
- [Luiz Henrique](/program/luiz-henrique)
- [Marcos Macedo](/program/marcos-macedo)
- [Misael Santos](/program/misael-santos)

Acesse o link e assista lá os *slides* das apresentações - *classifique como uma estrela* (***Star***) aquelas que gostou.

E **atenção**!!!

Fique ligado no site para as novidades, pois logo logo iremos publicar infos sobre as gravações e fotos das palestras.
