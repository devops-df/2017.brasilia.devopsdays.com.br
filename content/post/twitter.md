+++
categories = ["Announcement", "News"]
date = "2017-04-25T17:02:47-03:00"
description = "Também estamos no twitter!!!"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Social", "Media"]
title = "Twitter"

+++

eaê, Galera! Fala aí!

Com a aproximação do evento nós resolvemos criar uma conta no twitter para facilitar ainda mais o nosso contato.

Sigam-nos lá [`https://twitter.com/devopsdaysbsb`](https://twitter.com/devopsdaysbsb) e fiquemos antenados aos acontecimentos.

Também usem a ***hashtag*** [`#DevOpsDaysBrasilia`](https://twitter.com/search?q=%23DevOpsDaysBrasilia) para nos referenciar em mensagens e bombarmos no *top trends* do twitter.

Ajuda nóis aê, fi!

Grande abraço a vocês.

Nos encontramos no evento.
