+++
copalestrante_website = ""
copalestrante_name = ""
copalestrante_twitter = ""
copalestrante_photoUrl = ""
copalestrante_bio = ""

date = "2017-09-16T12:44:39-03:00"
categories = [
  "Talk",
  "Palestra",
]
description = "Adrian Goins"
draft = false
title = "Adrian Goins"
tags = [
  "DevOps",
  "Culture",
  "Automation",
  "Metrics",
  "Sharing",
]

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-warning">Gitlab: Own The Pipeline</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-warning">Sharing (compartilhamento)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- **Idioma da palestra**: Inglês
- ***slides***: [`https://speakerdeck.com/devopsdf/gitlab-own-the-pipeline-adrian-goins`](https://speakerdeck.com/devopsdf/gitlab-own-the-pipeline-adrian-goins)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
GitLab is, at its core, a tool for centrally managing Git repositories. As one might expect form a platform that provides this service, GitLab provides a robust authentication and authorization mechanism, groups, issue tracking, wiki, and snippets, along with public, internal, and private repositories.

Learn how to leverage Gitlab as the full stack solution for deploying apps, from code repo through build, to registry and automatic deployment.
