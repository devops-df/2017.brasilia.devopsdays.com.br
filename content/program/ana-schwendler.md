+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Sharing", "Compartilhamento" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Ana Schwendler"
description = "Ana Schwendler"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Operação Serenata de Amor</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**:<span class="text-primary">Culture (Cultura)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/anaschwendler/devopsdays`](https://speakerdeck.com/anaschwendler/devopsdays)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
A Operação Serenata de Amor surgiu a mais ou menos um ano e desde então tem tomado proporções muito interessantes. A Ana Schwendler vai falar um pouco sobre como o projeto chegou ate aqui e como o time se construiu multidisciplinar, remoto, que codifica, mantém, sustenta e evolui o projeto de forma organizada e orgânica.

{{< youtube id="ASrk2eUaEkc" autoplay="false" >}}
