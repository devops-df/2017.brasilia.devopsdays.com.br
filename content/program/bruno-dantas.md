+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Sharing", "Compartilhamento" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Bruno Dantas"
description = "Bruno Dantas"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-warning">DevSecOps: Adotando uma cultura de segurança ágil</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-warning">Sharing (compartilhamento)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- ***slides***: [`https://www.slideshare.net/dantas86/devsecops-adotando-uma-cultura-de-segurana-gil`](https://www.slideshare.net/dantas86/devsecops-adotando-uma-cultura-de-segurana-gil)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Conheça a experiência e os passos seguidos na adoção de um conjunto de práticas de segurança dentro da cultura DevOps. Tudo isso dentro da mais tradicional instituição financeira do país.


{{< youtube id="EJBpl6bkxp8" autoplay="false" >}}
