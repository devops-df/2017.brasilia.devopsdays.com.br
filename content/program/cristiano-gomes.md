+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Sharing", "Compartilhamento" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Cristiano Gomes"
description = "Cristiano Gomes"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Cloud DevOps Mobile</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
***Cloud DevOps Mobile***: entrega contínua para Apps Móveis usando serviços de nuvem

Implementando Práticas DevOps para Aplicativos Móveis usando Serviços em Nuvem

- Integração Contínua
- Testes Automatizados em milhares de dispositivos
- Distribuição Automatizada de Apps
- Telemetria, coleta de erros
- Análise de comportamento dos usuários
- Notificações
