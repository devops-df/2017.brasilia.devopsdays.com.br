+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Diego Dorgam"
description = "Diego Dorgam"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">ChatBots com Linguagem Natural OpenSource com Rocket.Chat</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/devopsdf/chatbots-com-linguagem-natural-opensource-com-rocket-dot-chat-diego-dorgan`](https://speakerdeck.com/devopsdf/chatbots-com-linguagem-natural-opensource-com-rocket-dot-chat-diego-dorgan)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Como implementar uma  cultura chatops usando chatbots opensource independentes de APIs de serviços externos, usando Rocket.Chat e hubot-natural.

{{< youtube id="z_aoDyedC-I" autoplay="false" >}}
