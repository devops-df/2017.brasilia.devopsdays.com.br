+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Fabio Rauber"
description = "Fábio Kaiser Rauber"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Docker, Rancher e a Hospedagem de Produtos Interlegis</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/docker-rancher-e-a-hospedagem-de-produtos-interlegis-fabio-rauber`](https://speakerdeck.com/devopsdf/docker-rancher-e-a-hospedagem-de-produtos-interlegis-fabio-rauber)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Como o Docker e o Rancher facilitaram o trabalho de criação e manutenção de serviços hospedados para Casas Legislativas no Interlegis.

{{< youtube id="zeWGYLLHCqM" autoplay="false" >}}
