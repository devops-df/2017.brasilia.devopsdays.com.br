+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Culture" ]
copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""
date = "2017-09-19T09:09:19-03:00"
title = "fernando ike"
description = "Fernando Ike"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Blameless: A culpa não é sua</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture (Cultura)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/devopsdf/blameless-a-culpa-nao-e-sua-blameless-post-mortems-fernando-ike`](https://speakerdeck.com/devopsdf/blameless-a-culpa-nao-e-sua-blameless-post-mortems-fernando-ike)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Ainda é muito praticado a cultura de responsabilizar as (outras) pessoas nas organizações por falhas, erros em incidentes e problemas. Uma das maneiras de mudar é implementar a cultura Blameless (Postmortem), ela tem o objetivo de não apontar para pessoas mas sim identificar e corrigir nos processos a causa da ocorrência. Blameless é muito importante para o aprendizado e crescimento das equipe nas organizações, ela é parte fundamental para que o Second e Third Way (The Three Way) seja bem implementado. Blameless também é aplicado em outras indústrias como aviação e engenharia.

{{< youtube id="XndMpRz6jW4" autoplay="false" >}}
