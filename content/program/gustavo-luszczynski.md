+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Gustavo Luszczynski"
description = "Gustavo Luszczynski"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Desenvolvimento Ágil de Aplicações utilizando o OpenShift</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Nesta palestra iremos criar uma API REST do zero utilizando wildfly swarm, jpa, bean validation, git, swagger, swagger-ui e banco de dados. Logo em seguida, iremos colocá-la em um container local por meio do Openshift. E por fim, adicionaremos a aplicação em um pipeline com Jenkins, slaves dinâmicos, estratégias de blue green deployment, testes de performance e testes de integração. Tudo isso executando em um ambiente do Google Cloud + Openshift.
