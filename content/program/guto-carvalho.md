+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Culture", "Cultura" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Guto Carvalho"
description = "José Augusto"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">DevOps State of Union 2017</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture (Cultura)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Iniciante
- ***slides***: [`https://speakerdeck.com/gutocarvalho/devops-state-of-the-union-2017`](https://speakerdeck.com/gutocarvalho/devops-state-of-the-union-2017)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
A ideia dessa palestra é fazer uma rápida introdução à DevOps e mostrar o resultados em números dos últimos  8 anos do movimento DevOps no mundo.

{{< youtube id="AwgzvLWWwJM" autoplay="false" >}}
