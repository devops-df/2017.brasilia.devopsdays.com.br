+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Igor Souza"
description = "Igor Souza"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">"You build it, you run it" - Mantendo sua infraestrutura como parte da aplicação!</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- ***slides***: [`https://pt.slideshare.net/IgorSouza70/you-build-it-you-run-it-81681281`](https://pt.slideshare.net/IgorSouza70/you-build-it-you-run-it-81681281)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Muito se fala sobre desenvolvimento ágil, integração continua e deploy continuo para aplicações. Mas do que tudo isso adianta se a sua infraestrutura não tiver a capacidade de se transformar na mesma velocidade que a sua aplicação? E se sua equipe pudesse ser responsável por gerir sua própria infraestrutura, diretamente do repositório da sua aplicação, sem depender de tickets ou emails. Vamos ver como algumas ferramentas de DevOps podem empoderar os times de devs através da automação.

{{< youtube id="l_f8X-lzd38" autoplay="false" >}}
