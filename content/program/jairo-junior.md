+++
copalestrante_website = ""
copalestrante_name = ""
copalestrante_twitter = ""
date = "2017-09-16T12:44:39-03:00"
categories = [
  "Talk",
  "Palestra",
]
description = "Jairo da Silva Junior"
copalestrante_photoUrl = ""
draft = false
copalestrante_bio = ""
title = "jairo junior"
tags = [
  "DevOps",
  "Culture",
  "Automation",
  "Metrics",
  "Sharing",
]

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">Testes de Infraestrutura na Prática</span>
<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text text-primary">Culture (Cultura)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Avançado
- ***slides***: [`https://docs.google.com/presentation/d/e/2PACX-1vTOombh0gxb1CMTpYRHwjukXImrn9omEQzYzzZS0in81jByagf5P42-D1vedZNMKqIVR-ZDC-G4pBGH/pub?start=false&loop=false&delayms=3000`](https://docs.google.com/presentation/d/e/2PACX-1vTOombh0gxb1CMTpYRHwjukXImrn9omEQzYzzZS0in81jByagf5P42-D1vedZNMKqIVR-ZDC-G4pBGH/pub?start=false&loop=false&delayms=3000)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Como as práticas de testes tem influenciado o desenvolvimento de infraestrutura? Quais práticas e ferramentas a comunidade de Ansible e Puppet tem adotado? Quais principais diferenças de *estratégia* e o que muda no mundo imutável dos containers.

Saindo dos testes unitários, integração e aceitação até chegarmos em práticas mais modernas como testes de resiliência (chaos engineering). Entenda como testes podem ajudar a amplificiar seus ciclos de feedback, reduzir MTTR e aumentar sua velocidade de desenvolvimento.

{{< youtube id="OZnxSEXD3Tk" autoplay="false" >}}
