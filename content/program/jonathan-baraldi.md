+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Sharing", "Compartilhamento" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Jonathan Baraldi"
description = "Jonathan Dias Baraldi"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">DevOps com Kubernetes</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Iremos fazer um overview sobre o Kuberentes. Falar sobre a arquitetura, sobre o que é o Kubernetes, o que podemos fazer com ele, e os problemas que ele resolve.
Ao final iremos rodar ao vivo algumas demos, mostrando: Autoscaling horizontal, Explicando Replication Controller, Helthcheck - Liveness probeness, Rolling update e Jobs
