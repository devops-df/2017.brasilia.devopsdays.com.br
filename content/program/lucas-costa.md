+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Lucas Costa"
description = "Lucas Moreira Rodrigues Costa"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">A arquitetura de monitoramento da plataforma de vídeos da Globo.com</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/a-arquitetura-de-monitoramento-da-plataforma-de-videos-da-globo-dot-com-lucas-costa`](https://speakerdeck.com/devopsdf/a-arquitetura-de-monitoramento-da-plataforma-de-videos-da-globo-dot-com-lucas-costa)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Nessa palestra pretendo apresentar a arquitetura de monitoramento da plataforma de vídeos da Globo.com, que constitui um dos componentes mais críticos e estratégicos da empresa.

Pretendo apresentar seus componentes principais (desde o player até os software de visualização de dados) e como se foi sua evolução ao longo dos últimos meses.

Com um grande enfoque na arquitetura da solução, pretendo apresentar os desafios, as razões por trás das escolhas que fizemos, os tradeoffs das implementações e os próximos passos.

Alguns dos componentes que serão abordados serão:

- o player de vídeos da Globo.com
- as métricas de usuário (coletadas via client-side)
- as métricas de sistema (coletadas via server-side)
- Kafka, como barramento de streaming de mensagens
- ELK, para coleta e armazenagem de métricas
- Parquet, para Big-Data
- Graphite, para consultas de séries temporais
- Grafana, para visualização de dados

{{< youtube id="WoD7bNE1CG8" autoplay="false" >}}
