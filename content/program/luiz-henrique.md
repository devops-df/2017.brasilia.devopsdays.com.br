+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Sharing", "Compartilhamento" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Luiz Henrique"
description = "Luiz Henrique de Souza"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Automação de processos em empresas tradicionais</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- ***slides***: [`https://pt.slideshare.net/LuizHenriquedeSouza/devops-e-orchestrator-automao-de-processos-em-empresas-tradicionais`](https://pt.slideshare.net/LuizHenriquedeSouza/devops-e-orchestrator-automao-de-processos-em-empresas-tradicionais)
<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Vou mostrar como implementamos uma cultura de automação de processos de infraestrutura, segurança e deploy em uma empresa tradicional.
Demonstrar exemplos de métricas que podem ser coletadas para evidenciar os ganhos obtidos, sejam eles do ponto de vista de governança, processos e também financeiros. Reduzimos quase 500k reais com automação somente no ano de 2016.
Falaremos sobre os principais desafios e quais foram os "atalhos do campo" para chegar ao sucesso.

{{< youtube id="1usRznUguN8" autoplay="false" >}}
