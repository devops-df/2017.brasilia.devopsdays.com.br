+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Sharing", "Compartilhamento" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Marcos Macedo"
description = "Marcos Macedo"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-warning">Fazendo entregas continuas no governo federal</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-warning">Sharing (compartilhamento)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/devopsdf/entregas-continuas-no-governo-federal-marcos-macedo`](https://speakerdeck.com/devopsdf/entregas-continuas-no-governo-federal-marcos-macedo)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Em um órgão do primeiro escalão do governo federal, estamos aplicando um modelo de DevOps com entregas continuas a cerca de 2 anos. Com uma média de uma entrega a cada 2,5 dias, a nosso experiência demonstra que este modelo não só é vantajoso como traz muitos benefícios. Nesta apresentação explicaremos algumas das técnicas  DevOps utilizadas, como Canary Releases, Modelo de Testes, Monitoração Continua e Gerenciamento Contínuo Baseado no Prince 2.

{{< youtube id="OkeX4ugszPE" autoplay="false" >}}
