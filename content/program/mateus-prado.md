+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Culture", "Cultura" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Mateus Prado"
description = "Mateus Prado"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-primary">DevOps Behind The Scenes</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-primary">Culture (Cultura)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
DevOps vem se mostrando uma metodologia adotada por muitas empresas de tecnologia dentro e fora do país. Muita tecnologia, novas ferramentas, processos e cultura. Muito se fala em se preocupar mais com cultura. Mas o que realmente devemos nos preocupar na adoção? Qual é o problema cultural que deixa o desafio ainda maior? E o cargo DevOps? Nesta talk, vamos apresentar uma visão na prática dos principais desafios e soluções encontradas, usando uma experiência durante 8 anos em 4 grandes empresas que ajudamos a introduzir DevOps como solução de problemas culturais e técnicos.
