+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Misael Santos"
description = "Misael da Silva Santos"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Pipelines Turbinadas: Impulsionando a cultura DevOps com Contêineres, Qualidade e Segurança na nuvem</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- ***slides***: [`https://speakerdeck.com/misaelssantos/devopsdays-pipelines-turbinadas`](https://speakerdeck.com/misaelssantos/devopsdays-pipelines-turbinadas)

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Muitas equipes encontram dificuldades em configurar e automatizar estágios mais avançados da sua pipeline, o que consome muito esforço e gera um grande obstáculo para uma adoção efetiva da Entrega Contínua. Como "turbinar" a adoção de pipelines de entrega que contemplem uma boa diversidade de testes, como testes funcionais, de desempenho e de segurança?
Nesta palestra iremos mostrar como utilizamos contêineres para disponibilizar para as equipes do Estaleiro, a nuvem privada do Serpro, ambientes pré-configurados com diversas ferramentas de testes funcionais, de desempenho e de segurança, que podem ser utilizados como serviço. Isto acelera a adoção das práticas de Entrega Contínua pelas equipes sem que elas percam o foco no que realmente interessa: gerar valor para o negócio.

{{< youtube id="IUyKhelFwWU" autoplay="false" >}}
