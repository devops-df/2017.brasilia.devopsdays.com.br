+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Sharing", "Compartilhamento" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-10-28T18:59:19-03:00"
title = "Rafael Benevides"
description = "Rafael Benevides"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-warning">Kubernetes for Docker users</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-warning">Sharing (compartilhamento)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- **Idioma da palestra**: Português

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Yes, Docker is great! We are all very aware of that but now it’s time to take the next step: wrapping it all and deploying to a production environment. For this scenario we need something more. For that “more” we have Kubernetes by Google - a container platform based on the same technology used to deploy billions of containers per month on Google’s infrastructure.

Ready to leverage your Docker skills and package your current Java app (WAR, EAR or JAR)? Come to this session to see how your current Docker skillset can be easily mapped to Kubernetes concepts and commands. And get ready to deploy your containers in production!
