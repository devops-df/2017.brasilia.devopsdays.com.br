+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Scott Moore"
description = "Scott Moore"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Security first: A SecDevOps approach to cloud building.</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário
- **Idioma da palestra**: Inglês

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
As more and more companies are making the shift from on-premises to public cloud; the standard approach to DevOps is evolving. From encryption, compliance and regulations like GDPR, security in the cloud has become a hot topic. Many DevOps-agile companies have hired dedicated staff to fulfill these requirements, often creating further siloes, complexity and cost. This presentation aims to highlight existing DevOps cultural approaches, tooling and how security can be wrapped in every facet of the build and release cycle and how to get sales and customer facing resources wrapped in. Scott will share real-life experiences working with customers at Microsoft Azure and Amazon Web Services and explain how focusing on ‘security first’ transformed their business from product ideation to creation.
