+++
draft = false
categories = [ "Talk", "Palestra" ]
tags = [ "DevOps", "Automation", "Automação" ]

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_photoUrl = ""
copalestrante_twitter = ""
copalestrante_website = ""

date = "2017-09-19T09:29:19-03:00"
title = "Vinicius Silva"
description = "Vinícius de Faria Silva"

+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Uso de nuvens públicas pelo setor público: como evitar o aprisionamento nos provedores de serviços de nuvem?</span>

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text-success">Automation (Automação)</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**: Intermediário

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
O interesse do setor público brasileiro em utilizar nuvens públicas tem sido cada vez maior, numa realidade de forte redução orçamentária, necessidade de aumento da eficência e agilidade e cobrança da sociedade por um governo mais digital. Ao mesmo tempo que oferece diversas oportunidades para o enfrentamento desta realidade o uso de nuvens públicas pelo setor público também apresenta um grande risco: o aprisionamento das organizações públicas nos provedores de serviços de nuvem. Nesta palestra vou apresentar um conjunto de práticas que buscam mitigar os principais riscos de aprisionamento das organizações públicas em provedores de serviços de nuvem.
