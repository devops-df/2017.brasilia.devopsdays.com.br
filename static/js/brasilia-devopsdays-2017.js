$(".menu-trigger").click(function(){

    $(this).find(".fa").toggleClass("fa-navicon");
    $(this).find(".fa").toggleClass("fa-close");

    $(".main-menu").toggle();
});

$(".show-info").click(function(){
    $(this).siblings(".description").toggleClass("description-show");
});

$(".description").click(function(){
    $(this).toggleClass("description-show");
});
